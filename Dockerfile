FROM gradle:latest
WORKDIR /executable
COPY build/libs/*-all.jar bin.jar
ENTRYPOINT ["java", "-jar",  "bin.jar"]
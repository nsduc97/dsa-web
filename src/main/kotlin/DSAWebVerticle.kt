import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.http.HttpServer
import io.vertx.core.impl.logging.LoggerFactory
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.kotlin.core.http.httpServerOptionsOf

class DSAWebVerticle : AbstractVerticle() {
    companion object {
        const val DEFAULT_WEB_HOST = "0.0.0.0"
        const val DEFAULT_WEB_PORT = 8080
    }

    private val logger = LoggerFactory.getLogger(DSAWebVerticle::class.java.name)

    override fun start(startPromise: Promise<Void>?) {
        val server = createServer()
        val router = createRouter()

        server
            .requestHandler(router)
            .listen { ar ->
                if (ar.failed()) {
                    logger.error("Server DSA startup fail")
                    startPromise?.fail(ar.cause())
                }
                else
                    logger.info("Server DSA startup success !")
            }
    }

    private fun createServer() : HttpServer {
        val port : Int  = System.getenv("PORT").toIntOrNull() ?: DEFAULT_WEB_PORT
        logger.info("Server exposed port=$port")
        val options = httpServerOptionsOf(
            host = DEFAULT_WEB_HOST,
            port = port
        )
        return vertx.createHttpServer(options)
    }

    private fun createRouter(): Router {
        val router = Router.router(vertx)
        router.route().handler(StaticHandler.create())

        router.get("/*").handler { rc ->
            val uri = rc.request().uri()
            logger.info("HTTP GET $uri")
            rc.response().end(uri)
        }
        return router
    }
}
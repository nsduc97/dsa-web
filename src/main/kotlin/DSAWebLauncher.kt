import io.vertx.core.Launcher

class DSAWebLauncher: Launcher() {
    companion object {
        const val DEFAULT_DEPLOYMENT_INSTANCES = 1
    }
}
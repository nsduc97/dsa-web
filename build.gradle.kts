plugins {
    kotlin("jvm") version "1.4.21"
    id("io.vertx.vertx-plugin") version "1.2.0"
}

group = "io.nsduc"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("io.vertx:vertx-core")
    implementation("io.vertx:vertx-lang-kotlin")
    implementation("io.vertx:vertx-web")

    testImplementation(kotlin("test"))
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

vertx {
    mainVerticle = "DSAWebVerticle"
    vertxVersion = "4.1.0"

}